/*
    Infilisp eval-on-use object base implementation
    Copyright (C) 2024  Orisphera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "InfilispObEOU.hh"
InfilispOP InfilispObEOU::car(const InfilispOP self) const { return self.get().car(); }
InfilispOP InfilispObEOU::cdr(const InfilispOP self) const { return self.get().cdr(); }
InfilispOP InfilispObEOU::apply(const InfilispOP self, const InfilispOP other) const { return self.get().apply(other); }
InfilispOP InfilispObEOU::eval(const InfilispOP self) const { return self.get().eval(); }
long long InfilispObEOU::to_ll(const InfilispOP self) const { return self.get().to_ll(); }
bool InfilispObEOU::to_bool(const InfilispOP self) const { return self.get().to_bool(); }
