/*
    Infilisp normal object base implementation
    Copyright (C) 2024  Orisphera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "InfilispObNorm.hh"

InfilispOP InfilispObNorm::get(const InfilispOP self) const { return self; }
InfilispOP InfilispObNorm::apply(const InfilispOP self, const InfilispOP other) const { return self.eval().apply(other); }
InfilispOP InfilispObNorm::eval(const InfilispOP self) const { return self.car().apply(self.cdr().eval()); }
long long InfilispObNorm::to_ll(const InfilispOP self) const { return self.car().to_bool() + self.cdr().to_ll() * 2; }
bool InfilispObNorm::to_bool(const InfilispOP self) const { return self.car().to_bool() || self.cdr().to_bool(); }
