# Infilisp
This project is unfinished.

## Description
Infilisp is an esoteric programming language. It started from an attempt to make a language for a procedural code generator for a game. See [the page on Esolangs](https://esolangs.org/Infilisp) for more.

## Installation
This implementation is unfinished, so you can't install it yet.

## Usage
If the language is finished, the following will probably be a Hello, World! program with the default driver:

```
in: '(72 101 108 108 111 44 32 87 111 114 108 100 33)
```

More examples can be found on the Esolangs page.

## Support
For help, you can open an issue or use any other way to contact with me.

## Roadmap
Currently, only the base Infilisp object classes are implemented. Before it is ready, the standard types and library should be done. A build system and/or a Nix wrapper can also be added. Tests for fuzzing can also be useful.

## Contributing
I'm open to contributions, but they shouldn't break the language.

## Authors and acknowledgment
Currently, no one else has contributed to the project. However, I'd like to thank the Lisp authors for inspiration.

## License
This project is licensed under GNU GPL3+.
