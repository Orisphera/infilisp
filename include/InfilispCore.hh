/*
    Base header for Infilisp
    Copyright (C) 2024  Orisphera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include <memory>

class InfilispOP;

class InfilispObject {
    public:
        virtual InfilispOP get(const InfilispOP self) const = 0;
        virtual InfilispOP car(const InfilispOP self) const = 0;
        virtual InfilispOP cdr(const InfilispOP self) const = 0;
        virtual InfilispOP apply(const InfilispOP self, InfilispOP other) const = 0;
        virtual InfilispOP eval(const InfilispOP self) const = 0;
        virtual bool to_bool(const InfilispOP self) const = 0;
        virtual long long to_ll(const InfilispOP self) const = 0;
};

class InfilispOP {
    private:
        const std::shared_ptr<const InfilispObject> ptr;
    public:
        InfilispOP(const std::shared_ptr<InfilispObject> ptr) : ptr(ptr) {}
        InfilispOP get() const {return this->ptr->get(*this);}
        InfilispOP car() const {return this->ptr->car(*this);}
        InfilispOP cdr() const {return this->ptr->cdr(*this);}
        InfilispOP apply(const InfilispOP other) const {return this->ptr->apply(*this, other);}
        InfilispOP eval() const {return this->ptr->eval(*this);}
        bool to_bool() const {return this->ptr->to_bool(*this);}
        long long to_ll() const {return this->ptr->to_ll(*this);}
};
