/*
    Infilisp eval-on-use object base header
    Copyright (C) 2024  Orisphera

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once
#include "InfilispCore.hh"

class InfilispObEOU : public InfilispObject {
    public:
        virtual InfilispOP car(const InfilispOP self) const override;
        virtual InfilispOP cdr(const InfilispOP self) const override;
        virtual InfilispOP apply(const InfilispOP self, const InfilispOP other) const override;
        virtual InfilispOP eval(const InfilispOP self) const override;
        virtual bool to_bool(const InfilispOP self) const override;
        virtual long long to_ll(const InfilispOP self) const override;
};
